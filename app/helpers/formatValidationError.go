package helpers

import(
	"strings"
	"fmt"
)

func FormatValidationErrorInput(err error) string{
	fmt.Println("Input Validation Error")
	if strings.Contains(err.Error(), "IDMember"){
		return "id_member is required"
	}else if strings.Contains(err.Error(), "IDReview"){
		return "id_review is required"
	}else if strings.Contains(err.Error(), "Username"){
		return "username is required"
	}else if strings.Contains(err.Error(), "Gender"){
		return "Gender is required"
	}else if strings.Contains(err.Error(), "SkinType"){
		return "skin_type is required"
	}else if strings.Contains(err.Error(), "SkinColor"){
		return "skin_color is required"
	}else if strings.Contains(err.Error(), "unmarshal"){
		if  strings.Contains(err.Error(), "InputLike.id_member"){
			return "id_member must has integer type"
		}else if strings.Contains(err.Error(), "InputLike.id_review"){
			return "id_review must has integer type"
		}else if strings.Contains(err.Error(), "UpdateMember.id_member"){
			return "id_member must has integer type"
		}else if strings.Contains(err.Error(), "UpdateMember.gender"){
			return "gender must has string type"
		}else if strings.Contains(err.Error(), "UpdateMember.skin_type"){
			return "skin_type must has string type"
		}else if strings.Contains(err.Error(), "UpdateMember.skin_color"){
			return "skin_color must has string type"
		}else if strings.Contains(err.Error(), "InputMember.username"){
			return "username must has string type"
		}else if strings.Contains(err.Error(), "InputMember.gender"){
			return "gender must has string type"
		}else if strings.Contains(err.Error(), "InputMember.skin_type"){
			return "skin_type must has string type"
		}else if strings.Contains(err.Error(), "InputMember.skin_color"){
			return "skin_color must has string type"
		}else{
			return "Something went wrong"
		}
	}else{
		return "Something went wrong"
	}
}