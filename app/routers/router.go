package routers

import(
	"github.com/gin-gonic/gin"
	"net/http"
	"k-style-test/app/handlers"
)


func InitRouter(){
	MemberHandler := handlers.NewMemberHandler()
	ProductHandler := handlers.NewProductHandler()
	LikeHandler := handlers.NewLikeHandler()
	r := gin.Default()
	api := r.Group("/api/v1")

	api.GET("/health", func(c *gin.Context){
		c.JSON(http.StatusOK, gin.H{
			"message": "I am ready!",
		})
	})
	api.GET("/member/health", MemberHandler.HealthMember)
	api.POST("/member", MemberHandler.CreateMember)
	api.PUT("/member", MemberHandler.UpdateMember)
	api.DELETE("/member", MemberHandler.DeleteMember)
	api.GET("/member/all", MemberHandler.GetAllMember)
	api.GET("/product/health", ProductHandler.HealthProduct)
	api.GET("/product/info", ProductHandler.GetProductByIdWithAllInfo)
	api.GET("/like/health", LikeHandler.HealthLike)
	api.POST("/like", LikeHandler.CreateLike)
	api.DELETE("/like", LikeHandler.DeleteLike)

	r.Run()
}