package handlers

import(
	"k-style-test/app/repositories"
	"k-style-test/app/helpers"
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"strconv"
)



type ProductHandler struct{
	repo repositories.ProductRepository
}

func NewProductHandler() *ProductHandler{
	return &ProductHandler{
		repositories.NewProductRepository(),
	}
}

func(h *ProductHandler) HealthProduct(c *gin.Context){
	c.JSON(http.StatusOK, gin.H{
		"message" : "Product Handler is ready!",
	})
}


func(h *ProductHandler) GetProductByIdWithAllInfo(c *gin.Context){
	id_product,_ := strconv.ParseUint(c.DefaultQuery("id_product","0"),10,64)
	id := uint(id_product)
	if id == 0{
		errors := "id_product cannot be null"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	product_repo := h.repo
	
	res, err := product_repo.GetProductByIdWithAllInfo(id)
	if err != nil {
		fmt.Println(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}
	if res.ID == 0{
		empty := "Data is empty"
		response := helpers.APIResponse("Success", http.StatusOK, empty)
		c.JSON(http.StatusOK, response)
		return
	}

	var data_review []map[string]interface{}
	for _, v1 := range res.ReviewProduct{
		count_like := 0
		for _,_ = range v1.LikeReview{
			count_like = count_like + 1
		}
		d := gin.H{
			"id": v1.ID,
			"member":gin.H{
				"username":v1.Member.Username,
				"gender":v1.Member.Gender,
				"skin_type":v1.Member.SkinType,
				"skin_color":v1.Member.SkinColor,
			},
			"review_description": v1.Description,
			"count_like":count_like,
		}
		data_review = append(data_review,d)
	}
	data := gin.H{
		"id":res.ID,
		"product_name":res.ProducName,
		"price":res.Price,
		"review":data_review,
	}

	response := helpers.APIResponse("Success", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}