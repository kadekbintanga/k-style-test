package handlers

import(
	"k-style-test/app/repositories"
	"k-style-test/app/resources"
	"k-style-test/app/helpers"
	"k-style-test/app/models"
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"strconv"
)



type MemberHandler struct{
	repo repositories.MemberRepository
}

func NewMemberHandler() *MemberHandler{
	return &MemberHandler{
		repositories.NewMemberRepository(),
	}
}

func(h *MemberHandler) HealthMember(c *gin.Context){
	c.JSON(http.StatusOK, gin.H{
		"message" : "Member Handler is ready!",
	})
}


func(h *MemberHandler) CreateMember(c *gin.Context){
	var req resources.InputMember
	err := c.ShouldBind(&req)
	if err != nil {
		fmt.Print(err)
		errors := helpers.FormatValidationErrorInput(err)
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	member_repo := h.repo

	check_username, err := member_repo.GetMemberByUsername(req.Username)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_username.ID != 0 {
		errors := "Username have been used"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	Member := models.Member{
		Username: req.Username,
		Gender: req.Gender,
		SkinType: req.SkinType,
		SkinColor: req.SkinColor,
	}

	res, err := member_repo.CreateMember(Member)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	data := gin.H{
		"id": res.ID,
		"username":res.Username,
		"skin_type": res.SkinType,
		"skin_color": res.SkinColor,
	}

	response := helpers.APIResponse("Success", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}


func(h *MemberHandler) UpdateMember(c *gin.Context){
	var req resources.UpdateMember
	err := c.ShouldBind(&req)
	if err != nil {
		fmt.Print(err)
		errors := helpers.FormatValidationErrorInput(err)
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	member_repo := h.repo
	
	check_id, err := member_repo.GetMemberById(req.IDMember)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_id.ID == 0{
		errors := "Id member not found"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	Member := models.Member{
		Username: req.Username,
		Gender: req.Gender,
		SkinType: req.SkinType,
		SkinColor: req.SkinColor,
	}

	_, err = member_repo.UpdateMember(req.IDMember, Member)
	// data := gin.H{
	// 	"id": res.ID,
	// 	"username":res.Username,
	// 	"skin_type": res.SkinType,
	// 	"skin_color": res.SkinColor,
	// }
	response := helpers.APIResponse("Success", http.StatusOK, "")
	c.JSON(http.StatusOK, response)
}

func(h *MemberHandler) DeleteMember(c *gin.Context){
	id_member,_ := strconv.ParseUint(c.DefaultQuery("id_member", "0"),10,64)
	id := uint(id_member)
	if id == 0{
		errors := "id_member cannot be null"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	member_repo := h.repo

	check_id, err := member_repo.GetMemberById(id)
	if err != nil {
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if check_id.ID == 0{
		errors := "id_member not found"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	err = member_repo.DeleteMember(id)
	if err != nil {
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	response := helpers.APIResponse("Success deleted data", http.StatusOK, "")
	c.JSON(http.StatusOK, response)
}


func(h *MemberHandler) GetAllMember(c *gin.Context){
	member_repo := h.repo
	res, err := member_repo.GetAllMember()
	if err != nil {
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	var data []map[string]interface{}
	for _,value := range res{
		d := gin.H{
			"id": value.ID,
			"username": value.Username,
			"gender":value.Gender,
			"skin_type":value.SkinType,
			"skin_color":value.SkinColor,
		}
		data = append(data,d)
	}

	response := helpers.APIResponse("Success", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}