package handlers

import(
	"k-style-test/app/repositories"
	"k-style-test/app/resources"
	"k-style-test/app/helpers"
	"k-style-test/app/models"
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	// "strconv"
)

type LikeHandler struct{
	repo_like repositories.LikeRepository
	repo_member repositories.MemberRepository
}

func NewLikeHandler() *LikeHandler{
	return &LikeHandler{
		repositories.NewLikeRepository(),
		repositories.NewMemberRepository(),
	}
}



func(h *LikeHandler) HealthLike(c *gin.Context){
	c.JSON(http.StatusOK, gin.H{
		"message" : "Like Handler is ready!",
	})
}


func(h *LikeHandler) CreateLike(c *gin.Context){
	var req resources.InputLike
	err := c.ShouldBind(&req)
	if err != nil {
		fmt.Print(err)
		errors := helpers.FormatValidationErrorInput(err)
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	member_repo := h.repo_member
	like_repo := h.repo_like
	check_member, err := member_repo.GetMemberById(req.IDMember)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_member.ID == 0{
		errors := "Id member not found"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	check_review, err := like_repo.GetRiviewById(req.IDReview)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_review.ID == 0{
		errors := "Id review not found"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	check_like, err := like_repo.GetLikeByMemberAndReviewId(req.IDMember, req.IDReview)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_like.ID != 0{
		errors := "Review has been liked"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	Like := models.LikeReview{
		MemberID: req.IDMember,
		ReviewProductID: req.IDReview,
	}

	_,err = like_repo.CreateLike(Like)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	response := helpers.APIResponse("Success", http.StatusOK, "")
	c.JSON(http.StatusOK, response)
}

func(h *LikeHandler) DeleteLike(c *gin.Context){
	var req resources.InputLike
	err := c.ShouldBind(&req)
	if err != nil {
		fmt.Print(err)
		errors := helpers.FormatValidationErrorInput(err)
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	like_repo := h.repo_like
	check_like, err := like_repo.GetLikeByMemberAndReviewId(req.IDMember, req.IDReview)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if check_like.ID == 0{
		errors := "User has not like the review product"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Bad Request", http.StatusBadRequest, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	err = like_repo.DeleteLike(req.IDMember, req.IDReview)
	if err != nil {
		fmt.Print(err)
		errors := "Something went wrong"
		errorMessage := gin.H{"Error message": errors}
		response := helpers.APIResponse("Internal Server Error", http.StatusInternalServerError, errorMessage)
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	response := helpers.APIResponse("Success", http.StatusOK, "")
	c.JSON(http.StatusOK, response)
}