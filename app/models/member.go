package models

import(
	"time"
	"gorm.io/gorm"
)

type Member struct{
	gorm.Model
	ID				uint		`json:"id" gorm:"primary_key"`
	Username		string		`json:"username"`
	Gender			string		`json:"gender"`
	SkinType		string		`json:"skin_type"`
	SkinColor		string		`json:"skin_color"`
	CreatedAt		time.Time	`json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt		time.Time	`json:"updated_at" gorm:"autoCreateTime,autoUpdateTime"`
}