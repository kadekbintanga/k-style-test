package models

import(
	"time"
	"gorm.io/gorm"
)

type Product struct{
	gorm.Model
	ID				uint			`json:"id" gorm:"primary_key"`
	ProducName		string			`json:"product_name"`
	Price			uint			`json:"price"`
	CreatedAt		time.Time		`json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt		time.Time		`json:"updated_at" gorm:"autoCreateTime,autoUpdateTime"`
	ReviewProduct	[]ReviewProduct	`gorm:"foreignKey:ProductID"`
}

type ReviewProduct struct{
	gorm.Model
	ID				uint			`json:"id" gorm:"primary_key"`
	ProductID		uint			`json:"product_id"`
	Product			Product			`gorm:"foreignKey:ProductID"`
	MemberID		uint			`json:"member_id"`
	Member			Member			`gorm:"foreignKey:MemberID"`
	Description		string			`json:"description"`
	CreatedAt		time.Time		`json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt		time.Time		`json:"updated_at" gorm:"autoCreateTime,autoUpdateTime"`
	LikeReview		[]LikeReview	`gorm:"foreignKey:ReviewProductID"`
}

type LikeReview struct{
	gorm.Model
	ID					uint				`json:"id" gorm:"primary_key"`
	ReviewProductID		uint				`json:"review_product_id"`
	ReviewProduct		ReviewProduct		`gorm:"foreignKey:ReviewProductID"`
	MemberID			uint				`json:"member_id"`
	Member				Member				`gorm:"foreignKey:MemberID"`
	CreatedAt			time.Time			`json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt			time.Time			`json:"updated_at" gorm:"autoCreateTime,autoUpdateTime"`
}