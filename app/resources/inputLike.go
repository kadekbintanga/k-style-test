package resources

type InputLike struct{
	IDMember		uint		`json:"id_member" binding:"required"`
	IDReview		uint		`json:"id_review" binding:"required"`
}