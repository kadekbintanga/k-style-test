package resources

type InputMember struct{
	Username		string		`json:"username" binding:"required"`
	Gender			string		`json:"gender" binding:"required"`
	SkinType		string		`json:"skin_type" binding:"required"`
	SkinColor		string		`json:"skin_color" binding:"required"`
}

type UpdateMember struct{
	IDMember		uint		`json:"id_member" binding:"required"`
	Username		string		`json:"username"`
	Gender			string		`json:"gender"`
	SkinType		string		`json:"skin_type"`
	SkinColor		string		`json:"skin_color"`
}