package repositories

import(
	"k-style-test/config"
	"k-style-test/app/models"
)

type LikeRepository interface{
	CreateLike(Like models.LikeReview)(models.LikeReview, error)
	GetRiviewById(id uint)(models.ReviewProduct, error)
	GetLikeByMemberAndReviewId(member_id uint, review_id uint)(models.LikeReview, error)
	DeleteLike(member_id uint, review_id uint)(error)
}

func NewLikeRepository() LikeRepository{
	return &dbConnection{
		connection: config.ConnectDB(),
	}
}

func(db *dbConnection) GetRiviewById(id uint)(models.ReviewProduct, error){
	var Review models.ReviewProduct
	connection := db.connection.Where("id = ?", id).Find(&Review)
	err := connection.Error
	if err != nil {
		return Review, err
	}
	return Review, nil
}

func(db *dbConnection) GetLikeByMemberAndReviewId(member_id uint, review_id uint)(models.LikeReview, error){
	var Like models.LikeReview
	connection := db.connection.Where("review_product_id = ? AND member_id = ?", review_id, member_id).Find(&Like)
	err := connection.Error
	if err != nil {
		return Like, err
	}
	return Like, nil
}


func(db *dbConnection) CreateLike(Like models.LikeReview)(models.LikeReview, error){
	err := db.connection.Save(&Like).Error
	if err != nil {
		return Like, err
	}
	return Like, nil
}

func(db *dbConnection) DeleteLike(member_id uint, review_id uint)(error){
	var Like models.LikeReview
	err := db.connection.Where("review_product_id = ? AND member_id = ?", review_id, member_id).Delete(&Like).Error
	if err != nil {
		return err
	}
	return nil
}