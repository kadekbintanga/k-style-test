package repositories

import(
	"k-style-test/config"
	"k-style-test/app/models"
)

type MemberRepository interface{
	GetMemberByUsername(username string)(models.Member, error)
	GetMemberById(id uint)(models.Member, error)
	CreateMember(Member models.Member)(models.Member, error)
	UpdateMember(id uint, Member models.Member)(models.Member, error)
	DeleteMember(id uint)error
	GetAllMember()([]models.Member, error)
}

func NewMemberRepository() MemberRepository{
	return &dbConnection{
		connection: config.ConnectDB(),
	}
}


func(db *dbConnection) GetMemberByUsername(username string)(models.Member, error){
	var Member models.Member
	connection := db.connection.Where("username = ?", username).Find(&Member)
	err := connection.Error
	if err != nil {
		return Member, err
	}
	return Member, nil
}

func(db *dbConnection) GetMemberById(id uint)(models.Member, error){
	var Member models.Member
	connection := db.connection.Where("id = ?", id).Find(&Member)
	err := connection.Error
	if err != nil {
		return Member, err
	}
	return Member, nil
}

func(db *dbConnection) GetAllMember()([]models.Member, error){
	var Member []models.Member
	connection := db.connection.Find(&Member)
	err := connection.Error
	if err != nil {
		return nil, err
	}
	return Member, nil
}

func(db *dbConnection) CreateMember(Member models.Member)(models.Member, error){
	err:= db.connection.Save(&Member).Error
	if err != nil {
		return Member, err
	}
	return Member, nil
}

func(db *dbConnection) UpdateMember(id uint, Member models.Member)(models.Member, error){
	err := db.connection.Model(&Member).Where("id = ?", id).Updates(&Member).Error
	if err != nil {
		return Member, err
	}
	return Member, nil
}

func(db *dbConnection) DeleteMember(id uint)error{
	var Member models.Member
	err := db.connection.Where("id = ?", id).Delete(&Member).Error
	if err != nil {
		return err
	}
	return nil
}