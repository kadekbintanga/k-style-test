package repositories

import(
	"k-style-test/config"
	"k-style-test/app/models"
)



type ProductRepository interface{
	GetProductByIdWithAllInfo(id uint)(models.Product, error)
}

func NewProductRepository() ProductRepository{
	return &dbConnection{
		connection: config.ConnectDB(),
	}
}


func(db *dbConnection) GetProductByIdWithAllInfo(id uint)(models.Product, error){
	var Product models.Product
	connection := db.connection.Preload("ReviewProduct.Member").Preload("ReviewProduct.LikeReview.Member").Where("id = ?", id).Find(&Product)
	err := connection.Error
	if err != nil {
		return Product, err
	}
	return Product, nil

}