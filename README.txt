Note :
    - This project is built with Gin Framework and ORM Gorm, so have to install the requirements
    - Install Gin -> run "go get github.com/gin-gonic/gin" in your terminal
    - Install Gorm -> run "go get gorm.io/gorm" and run "go get gorm.io/driver/mysql"


How to run project:
1. Create databases on your local with MySql
2. Create ".env" file in this project and copy configuration in ".env.example" to ".env"
3. Configure your database in ".env" file. Example:
    DB_USER=root
    DB_PASS=yourdbpassword
    DB_NAME=database's name
    DB_HOST=127.0.0.1 (for local)
    DB_PORT=3306
4. Run project with command "go run main.go" in your terminal
5. Project will migrate all table that is needed in this project
6. For test the project, you can use postman and import file  "PROJECT.postman_collection.json"(in this file project) to your postman app
7. Thank you