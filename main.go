package main

import(
	"fmt"
	"k-style-test/config"
	"k-style-test/app/routers"
	"gorm.io/gorm"
)

var(
	db *gorm.DB = config.ConnectDB()
)


func main(){
	fmt.Println("--------------------- Project started ---------------------")
	config.LoadEnv()
	config.MigrateDatabase(db)
	defer config.DisconnectDB(db)
	routers.InitRouter()
}